## This will creeate a VPC using terraform Modules
## This will create a cluster and subnet tags will be needed for cluster

locals {
  cluster_name = "i-tafader-cluster"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "3.19.0"

  name = "itafader-terraform_vpc"
  cidr = "10.10.0.0/16"

  azs             = ["us-east-2a", "us-east-2b", "us-east-2c"]
  private_subnets = ["10.10.4.0/24", "10.10.5.0/24", "10.10.6.0/24"]
  public_subnets  = ["10.10.101.0/24", "10.10.102.0/24", "10.10.103.0/24"]
  intra_subnets   = ["10.10.204.0/24", "10.10.205.0/24", "10.10.206.0/24"]


  enable_nat_gateway = true
  single_nat_gateway = true
  enable_dns_hostnames = true
# enable_vpn_gateway = false



 public_subnet_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = 1
  }

  private_subnet_tags = {
   "kubernetes.io/cluster/${local.cluster_name}" = "shared"
   "kubernetes.io/role/internal-elb"             = 1
  }



  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}

