resource "kubernetes_deployment_v1" "backend" {
  metadata {
    name = "backend"
    labels = {
      app = "MyExampleApp"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "MyExampleApp"
      }
    }

    template {
      metadata {
        labels = {
          app = "MyExampleApp"
        }
      }

      spec {
        container {
          image = "itafader/backend:${var.backend_tag}"
          name  = "backend"

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}

# This will create a service for deployment

resource "kubernetes_service_v1" "backend_svc" {
  metadata {
    name = "backend"
  }
  spec {
    selector = {
      app = kubernetes_deployment_v1.backend.metadata.0.labels.app
    }
   
    port {
      port        = 5000
      target_port = 5000
    }

    type = "LoadBalancer"
  }
}

