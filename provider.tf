## This will create a path as object in S3 bucket under the specified bucket name below.
## Kubernetes provider added and config path


terraform {
  backend "s3" {
    bucket = "itafader-terraform-state-bucket"
    key    = "modules-vpc/terraform.tfstate"
    region = "us-east-2"
  }

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.60.0"
    }

    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.19.0"
    }
  }
}


provider "aws" {
  region = "us-east-2"

}

provider "kubernetes" {
  config_path = "~/.kube/config"
}

provider "helm" {
  kubernetes {
  config_path = "~/.kube/config"
 }
}




